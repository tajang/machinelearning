This is a template for classification models.
The model first uses Standard Scalar to scale the data and utilizes these models:
1. Logistic Regression
2. K-Nearest Neighbors
3. Support Vector Machine
4. Kernel SVM
5. Naive Bayes
6. Decision Tree Classification
7. Random Forest Classification

and prints the model with highest result

NOTE: change the dataset name when importing the data.
